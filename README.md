# Automation Extreme Switches via Telnet

Python script for automation of extreme switches via Telnet.

## Installation

1. Install python modules in a virtualenv.


```bash
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```
2. Install python modules in local.


```bash
apt update
apt install python3-tqdm python3-colorama
```

## Usage

```bash
usage: manager_netdevice.py [-h] [-v] [-s] [-o] [-vv] [-d DEVICES] [-f FILE]

This is a Configuration Management System for Extreme Network Devices.

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program version
  -s, --sendCommand     send commands to network devices
  -o, --output          save output in devices/<host>.txt
  -vv, --verbosity      increase output verbosity - CAUTION: password in plain
                        text (telnet)
  -d DEVICES, --devices DEVICES
                        list of devices in CSV format containing <host,ip>
  -f FILE, --file FILE  list of commands to run on devices

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[Apache License](http://www.apache.org/licenses/)