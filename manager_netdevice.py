import os, time, sys
import argparse

# progress bar
from tqdm import tqdm
# print colored output
from colorama import Fore, Style
# import csv module to be open to easily open and parse csv files.
import csv
# import getpass so we can easily mask user input for passwords
import getpass
# Telnet
from telnetlib import Telnet

#########################################
##  Variables
#########################################

# Define the program description
text = 'This is a Configuration Management System for Extreme Network Devices.'

# Version of the Configuration Management System
version = 'Version 0.1'

# Output from devices
CONFIG_DIR = 'devices/'

# List of devices that contains our parsed csv file.
list_device = []

# Telnet config
port = 23
timeout = 100

# return Dictionary
ret={}

#########################################
##  Functions
#########################################

def makeQuestion(question,pattern):
    '''
    funcion to check question
    '''
    yes = {'Yes','Y','y'}
    no = {'No', 'Y', 'n'}

    while True:
        answer = input(question)
        if answer in yes or answer in '' and pattern in yes:
            return True
        elif answer in no or answer in '' and pattern in no:
            return False
        else:
            print ("Option not allowed")

def sendCommand(devices,file):
    '''
    send commands into network devices
    '''
    if not devices:
        print('Please devices is empty')
        return []
    if file:
        list_commands = open(file,'r')
    else:
        list_commands = [] 
        list_commands.append(input('Type the fisrt command: '))
        while True:
            if makeQuestion('Do you want to enter another command?[Y|n] ', 'Y'):
                list_commands.append(input('Type command: '))
            else:
                break
    print("\nPlease wait, in process...\n")
    for device in tqdm(devices):
        try:
            session = Telnet(device['ip'], port, timeout)
            if args.verbosity:
                session.set_debuglevel(1)
            time.sleep(2)
            session.read_until(b"login: ")
            session.write(username.encode('ascii') + b"\n")
            time.sleep(2)
            session.read_until(b"password: ")
            session.write(password.encode('ascii') + b"\n")
            time.sleep(2)
            for commands in list_commands:
                session.write(commands.encode('ascii') + b"\n")
                time.sleep(2)
            session.write(b"exit\n")
            lastpost = session.read_all().decode('ascii')
            if args.output:
                if not os.path.exists(CONFIG_DIR):
                    os.makedirs(CONFIG_DIR)
                op=open (CONFIG_DIR+device['host']+".txt", "a").write(lastpost)
                ret.update({device['host']: "created file "+CONFIG_DIR+device['host']+".txt"})
            else:
                ret.update({device['host']: lastpost})
            session.close()
        except:
            if args.output:
                if not os.path.exists(CONFIG_DIR):
                    os.makedirs(CONFIG_DIR)
                op=open (CONFIG_DIR+device['host']+".txt", "a").write("Unable to connect!")
                ret.update({device['host']: "created file "+CONFIG_DIR+device['host']+".txt"})
            else:
                ret.update({device['host']: "Unable to connect!"})
            continue
    return ret



if __name__ == '__main__':
    # initiate the parser
    parser = argparse.ArgumentParser(description = text)
    parser.add_argument("-v", "--version", help="show program version", action="store_true")
    parser.add_argument("-s", "--sendCommand", help="send commands to network devices", action="store_true")
    parser.add_argument("-o", "--output", help="save output in devices/<host>.txt", action="store_true")
    parser.add_argument("-vv", "--verbosity", help="increase output verbosity - CAUTION: password in plain text", action="store_true")
    parser.add_argument("-d", "--devices", help="list of devices in CSV format containing <host,ip>")
    parser.add_argument("-f", "--file", help="list of commands to run on devices")
    args = parser.parse_args() 

    if args.version:
        print (version) 
    elif args.sendCommand:
        if args.devices:
            print (Fore.GREEN + "\n################################################")
            print (Fore.GREEN + "##   SEND COMMAND  - EXTREME NETWORK DEVICE   ##")
            print (Fore.GREEN + "################################################\n")
            print(Style.RESET_ALL)
            reader = csv.DictReader(open(args.devices, 'r'))
            #takes our object we created from the parsed csv and makes a dict from each line
            for line in reader:
                list_device.append(line)

            username = input("Username: ")
            password = getpass.getpass("Password: ")
            
            begin = time.strftime("%H:%M:%S")
            log = sendCommand(list_device,args.file)
            end = time.strftime("%H:%M:%S")
            print ("\nConfiguration Management System for Extreme Network devices -", time.strftime("%d-%m-%Y")) 
            print ("Begin:", begin)
            print ("End:", end)

            if log:
                for key in log:
                    print (Fore.GREEN + "\nDEVICE: %s \nOUTPUT:" % (key))
                    print (Fore.YELLOW + "%s" % (log[key]))
                    print (Fore.GREEN + "------------------------------------------")
            else:
                print ("No output")
        else:
            print ("required arguments: manager_netdevice.py -h | -- help")
    else:
        print ("required arguments: manager_netdevice.py -h | -- help")
